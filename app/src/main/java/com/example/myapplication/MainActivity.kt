package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var check1: CheckBox
    private lateinit var check2: CheckBox
    private lateinit var check3: CheckBox
    private lateinit var check4: CheckBox
    private lateinit var check5: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        check1 = findViewById(R.id.ch1)
        check2 = findViewById(R.id.ch2)
        check3 = findViewById(R.id.ch3)
        check4 = findViewById(R.id.ch4)
        check5 = findViewById(R.id.ch5)

    }
    
    fun mainfun(clickedView: View) {


        if (check1.isChecked && check2.isChecked && check3.isChecked && !check4.isChecked && !check5.isChecked) {

            Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
            
        } else {

            Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()

        }
        
    }
    
    
}